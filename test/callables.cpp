#include <gtest/gtest.h>

#include <microfsm/microfsm.hpp>

namespace {

enum state { FOO, BAR };

state func1(state) { return BAR; }

state func2(state, int, double) { return BAR; }

struct callable1
{
  state operator()(state) { return BAR; }
};

struct callable2
{
  state operator()(state, int, double) { return BAR; }
};

struct single_context
{
  single_context() {}

  state func1(state) { return BAR; }
  state func2(state) { return BAR; }
  state func3(state, int, double) { return BAR; }

private:
  // No copy!
  single_context(single_context const &) = delete;
};


} // anonymous namespace

TEST(callables, function)
{
  microfsm::fsm<state> fsm{ FOO, { BAR }};
  fsm.add_transition(FOO, func1, { BAR });
}

TEST(callables, function_with_params)
{
  microfsm::fsm<state, int, double> fsm{ FOO, { BAR }};
  fsm.add_transition(FOO, func2, { BAR });
}

TEST(callables, lambda)
{
  microfsm::fsm<state> fsm{ FOO, { BAR }};
  fsm.add_transition(FOO, [] (state) -> state { return BAR; }, { BAR });
}

TEST(callables, lambda_with_params)
{
  microfsm::fsm<state, int, double> fsm{ FOO, { BAR }};
  fsm.add_transition(FOO, [] (state, int, double) -> state { return BAR; }, { BAR });
}

TEST(callables, object)
{
  microfsm::fsm<state> fsm{ FOO, { BAR }};
  callable1 c;
  fsm.add_transition(FOO, c, { BAR });
}

TEST(callables, object_with_params)
{
  microfsm::fsm<state, int, double> fsm{ FOO, { BAR }};
  callable2 c;
  fsm.add_transition(FOO, c, { BAR });
}

TEST(callables, single_context)
{
  microfsm::fsm<state> fsm{ FOO, { BAR }};

  // A single context should be bound to two transition functions
  single_context ctx;
  using namespace std::placeholders;
  fsm.add_transition(FOO,
      std::bind(&single_context::func1, &ctx, _1), { BAR });
  fsm.add_transition(BAR,
      std::bind(&single_context::func2, &ctx, _1), { FOO });
}

TEST(callables, single_context_with_arguments)
{
  microfsm::fsm<state, int, double> fsm{ FOO, { BAR }};

  // A single context should be bound to two transition functions
  single_context ctx;
  using namespace std::placeholders;
  fsm.add_transition(FOO,
      std::bind(&single_context::func3, &ctx, _1, _2, _3), { BAR });
}
