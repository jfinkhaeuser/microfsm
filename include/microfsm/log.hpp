/**
 * Copyright (C) Jens Finkhaeuser and other microfsm contributors.
 * Licensed under the MIT +no-false-attribs License. See LICENSE.txt for
 * licensing details.
 */
#ifndef MICROFSM_LOG_HPP
#define MICROFSM_LOG_HPP

#include <functional>
#include <sstream>

namespace microfsm {
namespace log {

/**
 * Log functions take a single string argument and do not return
 * anything.
 */
using log_function = std::function<void (std::string const &)>;

/**
 * In order to avoid unnecessary string formatting, we want a logger that
 * either writes to a string buffer, or does nothing, depending on it's
 * setting.
 */
struct logger
{
  inline logger(log_function log_func)
    : m_log_func(log_func)
    , m_buffer{}
  {
  }

  inline ~logger()
  {
    if (m_log_func) {
      m_log_func(m_buffer.str());
    }
  }

  template <typename T>
  inline logger &
  operator<<(T const & t)
  {
    if (m_log_func) {
      m_buffer << t;
    }
    return *this;
  }


private:

  log_function      m_log_func;
  std::stringstream m_buffer;
};

}} // namespace microfsm::log

#endif //guard
